// 1. Logging output
alert('Hello World'); 

console.log('Hello World');
console.error('This is an error');
console.warn('This is a warning');


// 2. Edit variable
// let age = 30;
// age = 31;

//console.log(age);


// 3. Edit data type
const name = 'John';
const age = 30;
const rating = 3.5;
const isCool = true;
const x = null;
const y = undefined;
let z;

console.log(typeof z);


// 4. Edit string

console.log('My name is ' + name + ' and I am ' + age);
console.log(`My name is ${name} and I am ${age}`);

const s = 'Hello World';
// let val;
// (val diganti console.log)
console.log(s.length);
console.log(s.toUpperCase());
console.log(s.toLowerCase());
console.log(s.substring(0, 5));
console.log(s.split(''));


// 5. Edit arrays
const numbers = [1,2,3,4,5];
const fruits = ['apples', 'oranges', 'pears', 'grapes'];
console.log(numbers, fruits);
console.log(fruits[1]);

fruits[4] = 'blueberries';
fruits.push('strawberries');
fruits.unshift('mangos');
fruits.pop();

console.log(Array.isArray(fruits));
console.log(fruits.indexOf('oranges'));


// 6. Edit object literals
const person = {
    firstName: 'John',
    age: 30,
    hobbies: ['music', 'movies', 'sports'],
    address: {
      street: '50 Main st',
      city: 'Boston',
      state: 'MA'
    }
  }
  
  // Get single value
  console.log(person.firstName)
  // Get array value
  console.log(person.hobbies[1]);
  // Get embedded object
  console.log(person.address.city);
  // Add property
  person.email = 'jdoe@gmail.com';
  // Array of objects
  const todos = [
    {
      id: 1,
      text: 'Take out trash',
      isComplete: false
    },
    {
      id: 2,
      text: 'Dinner with wife',
      isComplete: false
    },
    {
      id: 3,
      text: 'Meeting with boss',
      isComplete: true
    }
  ];
  
  console.log(todos[1].text);
  console.log(JSON.stringify(todos));


// 7. Edit loops
// for
for(let i = 0; i <= 10; i++){
    console.log(`For Loop Number: ${i}`);
  }
  // while
  let i = 0
  while(i <= 10) {
    console.log(`While Loop Number: ${i}`);
    i++;
  }
  // loop dengan arrays
  // for loop
  for(let i = 0; i < todos.length; i++){
    console.log(` Todo ${i + 1}: ${todos[i].text}`);
  }
  // for  ... of loop
  for(let todo of todos) {
    console.log(todo.text);
  }
  

// 8. Edit high order array methods 
  // forEach() - Loops through array
  todos.forEach(function(todo, i, myTodos) {
    console.log(`${i + 1}: ${todo.text}`);
    console.log(myTodos);
  });
  
  // map() - Loop through and create new array
  const todoTextArray = todos.map(function(todo) {
    return todo.text;
  });
  
  console.log(todoTextArray);
  
  // filter() - Returns array based on condition
  const todo1 = todos.filter(function(todo) {
    // Return only todos where id is 1
    return todo.id === 1; 
  });


// 9. Edit Conditionals
// Simple If/Else Statement
const q = 30;

if(q === 10) {
  console.log('q is 10');
} else if(q > 10) {
  console.log('q is greater than 10');
} else {
  console.log('q is less than 10')
}

// Switch
color = 'blue';

switch(color) {
  case 'red':
    console.log('color is red');
  case 'blue':
    console.log('color is blue');
  default:  
    console.log('color is not red or blue')
}

// Ternary operator / Shorthand if
const w = color === 'red' ? 10 : 20;


// 10. Edit Functions
// function greet(greeting = 'Hello', name) {
//     if(!name) {
//       // console.log(greeting);
//       return greeting;
//     } else {
//       // console.log(`${greeting} ${name}`);
//       return `${greeting} ${name}`;
//     }
//   }


// 11. Edit arrow functions
const greet = (greeting = 'Hello', name = 'There') => `${greeting} ${name}`;
console.log(greet('Hi'));


// 12. OOP
// Constructor Function
function Person(firstName, lastName, dob) {
    // Set object properties
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob); 
    // this.getBirthYear = function(){
    //   return this.dob.getFullYear();
    // }
    // this.getFullName = function() {
    //   return `${this.firstName} ${this.lastName}`
    // }
  }
  // Get Birth Year
  Person.prototype.getBirthYear = function () {
    return this.dob.getFullYear();
  }
  // Get Full Name
  Person.prototype.getFullName = function() {
    return `${this.firstName} ${this.lastName}`
  } 
  // Instantiate an object from the class
  const person1 = new Person('John', 'Doe', '7-8-80');
  const person2 = new Person('Steve', 'Smith', '8-2-90');
  
  console.log(person2);
  
  console.log(person1.getBirthYear());
  console.log(person1.getFullName());
  
  // Built in constructors
  const nama = new String('Kevin');
  console.log(typeof nama); 
  const num = new Number(5);
  console.log(typeof num); 
  

// 13. Edit ES6 classes
class orang {
    constructor(firstName, lastName, dob) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.dob = new Date(dob);
    }
    // Get Birth Year
    getBirthYear() {
      return this.dob.getFullYear();
    }
    // Get Full Name
    getFullName() {
      return `${this.firstName} ${this.lastName}`
    }
  }
  
  const orang1 = new orang('John', 'Doe', '7-8-80');
  console.log(orang1.getBirthYear());


// 14. Edit element selectors
// Single Element Selectors
console.log(document.getElementById('my-form'));
console.log(document.querySelector('.container'));
// Multiple Element Selectors
console.log(document.querySelectorAll('.item'));
console.log(document.getElementsByTagName('li'));
console.log(document.getElementsByClassName('item'));

const items = document.querySelectorAll('.item');
items.forEach((item) => console.log(item));


// 15. Edit manipulating the dom
// const ul = document.querySelector('.items');
// // ul.remove();
// // ul.lastElementChild.remove();
// ul.firstElementChild.textContent = 'Selamat Datang';
// ul.children[1].innerText = 'Kepada';
// ul.lastElementChild.innerHTML = 'Siapa yaa';
const btn = document.querySelector('.btn');
btn.style.background = '929eaa';


// 16. Edit events
// Mouse Event
// btn.addEventListener('click', e => {
//     e.preventDefault();
//     console.log(e.target.className);
//     document.getElementById('my-form').style.background = '#ccc';
//     document.querySelector('body').classList.add('bg-dark');
//     ul.lastElementChild.innerHTML = '<h1>Changed</h1>';
//   });
  


// 17. Edit user from script

// Put DOM elements into variables
const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

// Listen for form submit
myForm.addEventListener('submit', onSubmit);

function onSubmit(e) {
  e.preventDefault();
  
  if(nameInput.value === '' || emailInput.value === '') {
    // alert('Please enter all fields');
    msg.classList.add('error');
    msg.innerHTML = 'Please enter all fields';

    // Remove error after 3 seconds
    setTimeout(() => msg.remove(), 3000);
  } else {
    // Create new list item with user
    const li = document.createElement('li');

    // Add text node with input values
    li.appendChild(document.createTextNode(`${nameInput.value}: ${emailInput.value}`));

    // Add HTML
    // li.innerHTML = `<strong>${nameInput.value}</strong>e: ${emailInput.value}`;

    // Append to ul
    userList.appendChild(li);

    // Clear fields
    nameInput.value = '';
    emailInput.value = '';
  }
}